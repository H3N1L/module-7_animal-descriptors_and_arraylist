public class Budgie extends Animal{

    public Budgie(String name) {
        super(name, "Budgie", 2);
        this.name = name;
    }
}
