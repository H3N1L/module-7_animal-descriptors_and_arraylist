import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Horse horse = new Horse("Marty");

        Budgie budgie = new Budgie("Tweety");

        Snake snake = new Snake("Severus");


        ArrayList<Animal> animals = new ArrayList<>();
        animals.add(horse);
        animals.add(budgie);
        animals.add(snake);

        for(Animal a : animals) {
            a.describe();
        }
    }
}
