public class Animal {
    protected String name;
    protected String species;
    protected int numLegs;

    public Animal(String name, String species, int numLegs) {
        this.name = name;
        this.species = species;
        this.numLegs = numLegs;
    }

    public void describe() {
        if (numLegs == 0) {
            System.out.println("My name is " + name + ", I am a " + species + " and I have no legs.");
        } else {
            System.out.println("My name is " + name + ", I am a " + species + " and I have " + numLegs + " legs.");
        }
    }
}
